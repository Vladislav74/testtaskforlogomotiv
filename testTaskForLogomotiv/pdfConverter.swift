//
//  pdfConverter.swift
//  testTaskForLogomotiv
//
//  Created by Владислав on 07.09.16.
//  Copyright © 2016 vladislav. All rights reserved.
//

import Foundation
import UIKit

class pdfConverter {
    
    class func createPDFFromView(aView: UIView, saveToDocumentsWithFileName fileName: String)
    {
        let pdfData = NSMutableData()
        UIGraphicsBeginPDFContextToData(pdfData, aView.bounds, nil)
        UIGraphicsBeginPDFPage()
        
        guard let pdfContext = UIGraphicsGetCurrentContext() else { return }
        
        aView.layer.renderInContext(pdfContext)
        UIGraphicsEndPDFContext()
        
        if let documentDirectories = NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true).first {
            let documentsFileName = documentDirectories + "/" + fileName
            pdfData.writeToFile(documentsFileName, atomically: true)
        }
    }
    
}
