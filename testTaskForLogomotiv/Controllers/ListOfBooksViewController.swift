//
//  ListOfBooksViewController.swift
//  testTaskForLogomotiv
//
//  Created by Владислав on 06.09.16.
//  Copyright © 2016 vladislav. All rights reserved.
//

import UIKit

class ListOfBooksViewController: UITableViewController {

    @IBOutlet var booksTable: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(ListOfBooksViewController.updateBooksTable(_:)), name:"ListOfBooksWasUpdated", object: nil)
    }
    
    func updateBooksTable(notification: NSNotification) {
       self.booksTable.reloadData()
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return DataEngine.shared.listOfBooks.count
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cellId = "Book"
        let cell = tableView.dequeueReusableCellWithIdentifier(cellId, forIndexPath: indexPath)
        
        let bookName = DataEngine.shared.listOfBooks[indexPath.row].name
        cell.textLabel?.text = bookName
        return cell
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
        if(segue.identifier == "bookDetails") {
        
           let vc = segue.destinationViewController as! BookDetailsViewController
           
           let row = self.booksTable.indexPathForSelectedRow!.row
           print(row)
            
           vc.bookInfo = DataEngine.shared.listOfBooks[row]
        }
    }
    
    deinit {
       NSNotificationCenter.defaultCenter().removeObserver(self)
    }
}
