//
//  NewBookAdditionViewController.swift
//  testTaskForLogomotiv
//
//  Created by Владислав on 06.09.16.
//  Copyright © 2016 vladislav. All rights reserved.
//

import UIKit

class NewBookAdditionViewController: UIViewController, UITextFieldDelegate, UITextViewDelegate {

    @IBOutlet weak var bookNameTextField: UITextField!
    @IBOutlet weak var bookPriceTextField: UITextField!
    @IBOutlet weak var bookDescriptionTextField: UITextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        bookNameTextField.delegate = self
        bookPriceTextField.delegate = self
        bookDescriptionTextField.delegate = self
    }
    
    @IBAction func addNewBookButtonAction(sender: UIButton) {
        
        let newBook = Book()
        
        newBook.name = bookNameTextField.text
        newBook.price = bookPriceTextField.text
        newBook.description = bookDescriptionTextField.text
        
        DataEngine.shared.addNewBook(newBook)
        
        if let navigationController = self.navigationController
        {
            navigationController.popViewControllerAnimated(true)
        }
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true;
    }
    
    func textView(textView: UITextView, shouldChangeTextInRange range: NSRange, replacementText text: String) -> Bool {
        if(text == "\n") {
            textView.resignFirstResponder()
            return false
        }
        return true
    }
}
