//
//  BookDetailsViewController.swift
//  testTaskForLogomotiv
//
//  Created by Владислав on 06.09.16.
//  Copyright © 2016 vladislav. All rights reserved.
//

import UIKit
import MessageUI

class BookDetailsViewController: UIViewController, MFMailComposeViewControllerDelegate {

    @IBOutlet weak var bookNameLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UITextView!
    @IBOutlet weak var bookDetailsView: UIView!
    
    var bookInfo : Book!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        bookNameLabel.text = bookInfo.name
        priceLabel.text = "Цена: \(bookInfo.price)" 
        descriptionLabel.text = bookInfo.description
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
        if(segue.identifier == "payment") {
            
            let vc = segue.destinationViewController as! PaymentViewController
            vc.bookName = bookInfo.name
            vc.bookPrice = bookInfo.price
        }
    }

    @IBAction func sendDescriptionOnEmailButtonAction(sender: UIButton) {
        
        let mailComposeViewController = self.configuredMailComposeViewController()
        if MFMailComposeViewController.canSendMail() {
            self.presentViewController(mailComposeViewController, animated: true, completion: nil)
        } else {
            self.showSendMailErrorAlert()
        }
    }
    
    func configuredMailComposeViewController() -> MFMailComposeViewController {
        let mailComposerVC = MFMailComposeViewController()
        mailComposerVC.mailComposeDelegate = self
        
        mailComposerVC.setToRecipients(["zhukov.vladisalv@gmail.com"])
        mailComposerVC.setSubject("описание для \(bookInfo.name)")
        mailComposerVC.setMessageBody("Описание книги в приложении!", isHTML: false)
        
        pdfConverter.createPDFFromView(self.bookDetailsView, saveToDocumentsWithFileName: "bookDescription")
        
        if let documentDirectories = NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true).first {
            let documentsFileName = documentDirectories + "/bookDescription"
            if let fileData = NSData(contentsOfFile: documentsFileName) {
                mailComposerVC.addAttachmentData(fileData, mimeType: "pdf", fileName: "bookDescription.pdf")
            }
        }
        return mailComposerVC
    }
    
    func showSendMailErrorAlert() {
        
        let alert = UIAlertController(title: "Не получилось отправить email", message: "проверте настройки вашего телефона", preferredStyle: .Alert)
        alert.addAction(UIAlertAction(title: "Ок", style: .Cancel, handler: nil))
        self.presentViewController(alert, animated: true, completion: nil)
    }
    
    // MARK: MFMailComposeViewControllerDelegate Method
    func mailComposeController(controller: MFMailComposeViewController, didFinishWithResult result: MFMailComposeResult, error: NSError?) {
        controller.dismissViewControllerAnimated(true, completion: nil)
    }
}
