//
//  PaymentViewController.swift
//  testTaskForLogomotiv
//
//  Created by Владислав on 06.09.16.
//  Copyright © 2016 vladislav. All rights reserved.
//

import UIKit

class PaymentViewController: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var bookNameLabel: UILabel!
    @IBOutlet weak var bookPriceLabel: UILabel!
    
    @IBOutlet weak var cardNumberTextField: UITextField!
    @IBOutlet weak var validityTextField: UITextField!
    @IBOutlet weak var ccvTextField: UITextField!
    
    var bookName : String!
    var bookPrice : String!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        bookNameLabel.text = bookName
        bookPriceLabel.text = bookPrice
        
        cardNumberTextField.delegate = self
        validityTextField.delegate = self
        ccvTextField.delegate = self
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
        cardNumberTextField.placeholder = DataEngine.shared.userPayInfo.cardNumber
        validityTextField.placeholder =  DataEngine.shared.userPayInfo.validity
        ccvTextField.placeholder =  DataEngine.shared.userPayInfo.ccv
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true;
    }
    
    @IBAction func buyButtonAction(sender: UIButton) {
       
        if(cardNumberTextField.text != DataEngine.shared.userPayInfo.cardNumber) {
            
            let newUserPayInfo = UserPayInfo()
            newUserPayInfo.cardNumber = cardNumberTextField.text
            newUserPayInfo.validity = validityTextField.text
            newUserPayInfo.ccv = ccvTextField.text
            DataEngine.shared.saveUserPayInfo(newUserPayInfo)
        }
        
        let alert = UIAlertController(title: "Отлично! Вы купили книгу:", message: bookName, preferredStyle: .Alert)
        
        alert.addAction(UIAlertAction(title: "OK", style: .Cancel, handler: nil))
        presentViewController(alert, animated: true, completion: nil)
    }
}
