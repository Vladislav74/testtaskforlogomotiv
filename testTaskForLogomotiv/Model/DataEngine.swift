//
//  DataEngine.swift
//  testTaskForLogomotiv
//
//  Created by Владислав on 06.09.16.
//  Copyright © 2016 vladislav. All rights reserved.
//

import Foundation
import SwiftyJSON

class Book {
    var name : String!
    var price : String!
    var description : String!
}

class UserPayInfo {
    var cardNumber : String!
    var validity : String!
    var ccv : String!
}


class DataEngine {
    
    static let shared = DataEngine()
    
    var listOfBooks = [Book]()
    var userPayInfo = UserPayInfo()
        
    func obtainBookListAndUserPayInfo() {
        
        let defaults = NSUserDefaults.standardUserDefaults()
        let listOfBooksAsString = defaults.stringForKey("ListOfBooks")
        let userPayInfoAsString = defaults.stringForKey("UserPayInfo")
        
        if let dataFromString = listOfBooksAsString!.dataUsingEncoding(NSUTF8StringEncoding, allowLossyConversion: false) {
            let json = JSON(data: dataFromString)
            
            for index in 0..<json.count {
                let book = Book()
                
                book.name =  json[index].dictionary!["name"]!.rawString()
                book.price = json[index].dictionary!["price"]!.rawString()
                book.description = json[index].dictionary!["description"]!.rawString()
                
                listOfBooks.append(book)
            }
        }
        
        if let dataFromString = userPayInfoAsString!.dataUsingEncoding(NSUTF8StringEncoding, allowLossyConversion: false) {
            let json = JSON(data: dataFromString)
            
            let userPayInfo = UserPayInfo()
            
            userPayInfo.cardNumber =  json[0].dictionary!["cardNumber"]!.rawString()
            userPayInfo.validity = json[0].dictionary!["validity"]!.rawString()
            userPayInfo.ccv = json[0].dictionary!["ccv"]!.rawString()
        }
    }
    
    func addNewBook(newBook : Book) {
        
       self.listOfBooks.append(newBook)
       
       var books = [NSDictionary]()
        
       for book in listOfBooks {
          books.append(["name":book.name!, "price":book.price!, "description":book.description!])
       }
        
       let listOfBooksAsJson = JSON(books)
       let listOfBooksAsString = listOfBooksAsJson.rawString()
        
       let defaults = NSUserDefaults.standardUserDefaults()
       defaults.setValue(listOfBooksAsString, forKey: "ListOfBooks")
        
       NSNotificationCenter.defaultCenter().postNotificationName("ListOfBooksWasUpdated", object: nil)
    }
    
    func saveUserPayInfo(payInfo : UserPayInfo) {
        userPayInfo = payInfo
        
        let payInfo = ["cardNumber":userPayInfo.cardNumber, "validity":userPayInfo.validity, "ccv":userPayInfo.ccv]
        let userPayInfoAsJson : JSON = [payInfo]
        let userPayInfoAsString = userPayInfoAsJson.rawString()
        
        let defaults = NSUserDefaults.standardUserDefaults()
        defaults.setValue(userPayInfoAsString, forKey: "UserPayInfo")
    }
    
    class func setStartingBookListAndUserPayInfo() {
        
        let book1 = ["name":"The Lord of the Rings", "price":"100", "description":"The Lord of the Rings is an epic high-fantasy novel written by English author J. R. R. Tolkien. The story began as a sequel to Tolkien's 1937 fantasy novel The Hobbit, but eventually developed into a much larger work. Written in stages between 1937 and 1949, The Lord of the Rings is one of the best-selling novels ever written, with over 150 million copies sold."]
        
        let book2 = ["name":"The Martian", "price":"134", "description":"The Martian is a 2011 science fiction novel written by Andy Weir. It was his debut novel under his own name. It was originally self-published in 2011; Crown Publishing purchased the rights and re-released it in 2014. The story follows an American astronaut, Mark Watney, as he becomes stranded alone on Mars in the year 2035 and must improvise in order to survive. The Martian, a film adaptation directed by Ridley Scott and starring Matt Damon, was released in October 2015."]
        
        let book3 = ["name":"Alice's Adventures in Wonderland", "price":"330", "description":"is an 1865 novel written by English mathematician Charles Lutwidge Dodgson under the pseudonym Lewis Carroll. It tells of a girl named Alice falling through a rabbit hole into a fantasy world populated by peculiar, anthropomorphic creatures. The tale plays with logic, giving the story lasting popularity with adults as well as with children.[1] It is considered to be one of the best examples of the literary nonsense genre. Its narrative course and structure, characters and imagery have been enormously influential in both popular culture and literature, especially in the fantasy genre."]
        
        let book4 = ["name":"Surely You're Joking, Mr. Feynman!", "price":"75", "description":"Surely You're Joking, Mr. Feynman!: Adventures of a Curious Character is an edited collection of reminiscences by the Nobel Prize-winning physicist Richard Feynman. The book, released in 1985, covers a variety of instances in Feynman's life. Some are lighthearted in tone, such as his fascination with safe-cracking, studying various languages, participating with groups of people who share different interests (such as biology or philosophy), and ventures into art and samba music. Others cover more serious material, including his work on the Manhattan Project (during which his first wife Arline Greenbaum died of tuberculosis) and his critique of the science education system in Brazil. The section Monster Minds describes his slightly nervous presentation of his graduate work on the Wheeler–Feynman absorber theory in front of Albert Einstein, Wolfgang Pauli and other major figures of the time."]
        
        let book5 = ["name":"Gone Girl", "price":"85", "description":"Gone Girl is a thriller novel by the writer Gillian Flynn. It was published by Crown Publishing Group in June 2012. The novel soon made the New York Times Best Seller list. The novel's suspense comes from the main character, Nick Dunne, and whether he is involved in the disappearance of his wife."]

        let listOfBooksAsJson : JSON = [book1, book2, book3, book4, book5]
        let listOfBooksAsString = listOfBooksAsJson.rawString()
        
        let payInfo = ["cardNumber":"0000 0000 0000 0000", "validity":"00/00", "ccv":"000"]
        let userPayInfoAsJson : JSON = [payInfo]
        let userPayInfoAsString = userPayInfoAsJson.rawString()
        
        //let domainName = NSBundle.mainBundle().bundleIdentifier!
        //NSUserDefaults.standardUserDefaults().removePersistentDomainForName(domainName)
        
        let defaults = NSUserDefaults.standardUserDefaults()
        let startSettings = ["ListOfBooks" : listOfBooksAsString!, "UserPayInfo" : userPayInfoAsString!]
        defaults.registerDefaults(startSettings)
    }    
}
